#ifndef __Timer6_H__
#define __Timer6_H__

#include "main.h"

//定义枚举类型
typedef enum
{
	TIMER0_1S     = (uint16_t)1,
	TIMER0_2S     = (uint16_t)2,
	TIMER0_3S     = (uint16_t)3,
	TIMER0_5S     = (uint16_t)5,
	TIMER0_10S    = (uint16_t)10,
	TIMER0_30S		= (uint16_t)30,
	TIMER0_1min   = (uint16_t)60,
	
}TIMER0_Value_t;

//定义结构体类型
typedef struct
{
  uint16_t volatile usMCU_Run_Timer;  //系统运行定时器
  uint16_t getSensor_Timer;
	
} Timer6_t;

/* extern variables-----------------------------------------------------------*/
extern Timer6_t  Timer6;

#endif
/********************************************************
  End Of File
********************************************************/