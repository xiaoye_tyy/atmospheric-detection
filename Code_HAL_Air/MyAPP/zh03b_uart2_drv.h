#ifndef	__ZH03B_UART2_DRV_H
#define __ZH03B_UART2_DRV_H

#include "main.h"


extern const  uint8_t AutoSendMode[9];
extern const uint8_t ResponseMode[9];
extern const  uint8_t QueryPM25Message[9];
uint8_t FucCheckSum(uint8_t *i,uint8_t ln);
uint8_t GetPMValues(uint8_t *arr,uint16_t *PM25,uint16_t *PM10,uint16_t *PM1dot0);


#endif