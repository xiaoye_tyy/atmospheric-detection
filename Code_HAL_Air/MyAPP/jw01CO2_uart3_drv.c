#include "jw01CO2_uart3_drv.h"


/**
  * 功能描述：求和校验
  * 函数说明：将数组前N个元素相加然后
  */
uint8_t FucCheckCO2Sum(uint8_t *arr,uint8_t ln)
{
	uint8_t i = 0;
	uint8_t TEMP = 0;
	for(i = 0;i<ln;i++)
	{
		TEMP += arr[i];
	}
	return TEMP;
}


uint8_t GetCO2Value(uint8_t *arr,uint16_t * CO2)
{
	//校验函数
	if(FucCheckCO2Sum(arr,5)==arr[5])
	{
		//printf("校验成功\r\n");
		*CO2 = arr[1]*256+arr[2];
		return 1;
	}
	else
	{
		printf("校验失败\n\r");
		return 0;
	}
}