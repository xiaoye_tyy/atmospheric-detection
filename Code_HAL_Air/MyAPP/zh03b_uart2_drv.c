#include "zh03b_uart2_drv.h"



//PM串口命令
const  uint8_t AutoSendMode[9] = {0xff, 0x01,0x78,0x40,0x00,0x00,0x00,0x00,0x47};//设置为主动发送模式
const uint8_t ResponseMode[9] = {0xff, 0x01,0x78,0x41,0x00,0x00,0x00,0x00,0x46};//设置为应答模式
const  uint8_t QueryPM25Message[9] = {0xff, 0x01,0x86,0x00,0x00,0x00,0x00,0x00,0x79};//获取PM浓度

/**
  * 功能描述：求和校验（取发送、接收协议的1-7的和取反+1）
  * 函数说明：将数组的元素1-倒数第二个元素相加后取反+1（元素个数必须大于2）
  */
uint8_t FucCheckSum(uint8_t *i,uint8_t ln)
{
	uint8_t j,tempq = 0;
	i+=1;
	for(j = 0;j<(ln-2);j++)
	{
		tempq+=*i;
		i++;
	}
	tempq = (~tempq)+1;
	return tempq;
	
}


/**
  * 功能描述：获取PM2.5 PM1.0 PM10浓度
  * 函数说明：先判断命令，然后在依次读取PM2.5 PM10 PM1.0的浓度
	* 函数返回值：1：正确读取浓度
	*							0：读取浓度失败
  */

uint8_t GetPMValues(uint8_t *arr,uint16_t *PM25,uint16_t *PM10,uint16_t *PM1dot0)
{
	
	//判断命令
	if(arr[1]==0x86)
	{
		*PM25 = ((uint16_t)arr[2]<<8)|(uint16_t )arr[3];
		*PM10 = ((uint16_t)arr[4]<<8)|(uint16_t) arr[5];
		*PM1dot0 = ((uint16_t)arr[6]<<8)|(uint16_t)arr[7];
		return 1;
	}
	else
	{
		printf("命令不正确\n");
		return 0;
	}
	
}