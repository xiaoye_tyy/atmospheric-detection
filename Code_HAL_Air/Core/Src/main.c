/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "i2c.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stdio.h"
#include "sht30_i2c_drv.h"
#include "BH1750_i2c_drv.h"
#include "BMP180_i2c_drv.h"
#include "zh03b_uart2_drv.h"
#include "jw01CO2_uart3_drv.h"
#include "timer6.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define UART5_RS485_SEND_MODE    HAL_GPIO_WritePin(RS485EN1_GPIO_Port,RS485EN1_Pin,GPIO_PIN_SET); HAL_Delay(2)
#define UART5_RS485_RECV_MODE    HAL_GPIO_WritePin(RS485EN1_GPIO_Port,RS485EN1_Pin,GPIO_PIN_RESET);HAL_Delay(2)
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
//CO2浓度缓存
uint8_t recvCO2_buf[6] = {0};
uint8_t SensorInform[200] = {0};
uint8_t CO2_RxFlag = 0;
uint8_t getSensorFlag = 0;
//颗粒物缓�??
uint8_t recvPM_buf[9] = {0};
uint8_t PM_RxFlag = 0;
uint16_t PM25,PM10,PM1dot0,CO2,Lux;
float temperature1,humidity,temperature2,pressure;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
void SHT30_Task(void);
void BH17h0_Task(void);
void TestCO2Task(void);
void testGetPMSensor(void);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */


/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C2_Init();
  MX_TIM6_Init();
  MX_USART1_UART_Init();
  MX_I2C1_Init();
  MX_USART3_UART_Init();
  MX_USART2_UART_Init();
  MX_UART5_Init();
  /* USER CODE BEGIN 2 */
	
	//SHT30初始�??
	SHT30_Reset();
    if(SHT30_Init() == HAL_OK)
        printf("sht30 init ok.\n");
    else
        printf("sht30 init fail.\n");
	//使能串口3�?2中断接收
  HAL_UART_Receive_IT(&huart3, (uint8_t*)recvCO2_buf, 6);
	HAL_UART_Receive_IT(&huart2, (uint8_t*)recvPM_buf, 9);
	//设置为应答模�?
	HAL_UART_Transmit_IT(&huart2, (uint8_t*)ResponseMode, 9);
	HAL_TIM_Base_Start_IT(&htim6);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */


    if(getSensorFlag==1)
    {
      getSensorFlag = 0;
      SHT30_Task();
      HAL_Delay(50);
      BH17h0_Task();
      HAL_Delay(50);
      GetRawData(&pressure);
      HAL_Delay(50);
      TestCO2Task();
      HAL_Delay(50);
      testGetPMSensor();
      HAL_Delay(50);
      sprintf(SensorInform,"{\"temperature\":%.2f,\"humidity\":%.2f,\"Lux\":%d,\"CO2\":%d,\"PM2.5\":%d,\"PM1.0\":%d,\"PM10\":%d,\"pressure\":%.2f}",temperature1,humidity,Lux,CO2,PM25,PM1dot0,PM10,pressure);
      HAL_UART_Transmit_IT(&huart1, (uint8_t*)SensorInform, 200);

      UART5_RS485_SEND_MODE;//设置为发送模式
      HAL_UART_Transmit_IT(&huart5,(uint8_t*)SensorInform, 200);

      UART5_RS485_RECV_MODE;
    }


	}

  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */


int fputc(int ch,FILE* f)
{

	HAL_UART_Transmit(&huart1, (uint8_t *)&ch, 1, 0x000A);

	return ch;
}
//测试成功
void SHT30_Task(void)
{
	 uint8_t recv_dat[6] = {0};
  //  float temperature = 0.0;
  //  float humidity = 0.0;
	 
	 if(SHT30_Read_Dat(recv_dat) == HAL_OK)
		{
				if(SHT30_Dat_To_Float(recv_dat, &temperature1, &humidity)==0)
				{
						printf("temperature = %f, humidity = %f\n", temperature1, humidity);

				}
				else
				{
						printf("crc check fail.\n");
				}
		}
		else
		{
			printf("read data from sht30 fail.\n");
			SHT30_Reset();
			if(SHT30_Init() == HAL_OK)
					printf("sht30 init ok.\n");
			else
					printf("sht30 init fail.\n");
			
		}
}
//测试成功
void BH17h0_Task(void)
{
	
	Lux = Get_BH1750_Value();
	if(Lux!=0)
	{
		printf("Lux = %d\n", Lux);
	}
	else
	{
		printf("Read light fail \n");
	}
}




/* 中断回调函数 */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
    /* 判断是哪个串口触发的中断 */
    if(huart ->Instance == USART3)
    {
        //将接收到的数据发送给串口1
        //HAL_UART_Transmit_IT(&huart1, (uint8_t*)recvCO2_buf, 6);
				CO2_RxFlag = 1;
				
        //重新使能串口接收中断
        HAL_UART_Receive_IT(huart, (uint8_t*)recvCO2_buf, 6);
    }
		
		if(huart ->Instance == USART2)
    {
        //将接收到的数据发送给串口1
        //HAL_UART_Transmit_IT(&huart1, (uint8_t*)recvPM_buf, 9);
				PM_RxFlag = 1;
        //重新使能串口接收中断
        HAL_UART_Receive_IT(huart, (uint8_t*)recvPM_buf, 9);
    }
}


/*
	* @name   HAL_TIM_PeriodElapsedCallback
	* @brief  定时器中断回调函�?
	* @param  *htim -> 处理定时器的结构体指�?
	* @retval None      
*/
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	if(htim->Instance == htim6.Instance)
	{
		//程序支持运行，指示灯间隔1s闪烁
		if(++Timer6.usMCU_Run_Timer >= TIMER0_1S)
		{
			Timer6.usMCU_Run_Timer = 0;
      HAL_GPIO_TogglePin(LED2_GPIO_Port,LED2_Pin);
		}

    if(++Timer6.getSensor_Timer>=TIMER0_30S)
    {
      Timer6.getSensor_Timer = 0;
      HAL_GPIO_TogglePin(LED3_GPIO_Port,LED3_Pin);
      getSensorFlag = 1;
      
    }
	}
}


void testGetPMSensor()
{
	
	HAL_Delay(10);
	//发�?�串口命令读取串口信�?
	HAL_UART_Transmit_IT(&huart2, (uint8_t*)QueryPM25Message, 9);
	HAL_Delay(10);
	if(PM_RxFlag)
	{
		PM_RxFlag=0;
		//printf("已经获取到数据\r\n");
		
		//校验数据 
		if(FucCheckSum(recvPM_buf,9)==recvPM_buf[8])
		{
			//printf("数据校验成功\r\n");
			if(GetPMValues(recvPM_buf,&PM25,&PM10,&PM1dot0))
			{
				printf("PM 1.0=%d ug/m,PM2.5 = %d ug/m, PM10 = %d ug/m \r\n",PM1dot0,PM25,PM10);
			}
		}
		else
		{
			printf("数据校验失败\n");
		}
	}
	HAL_Delay(1000);
}



void TestCO2Task()
{
	if(CO2_RxFlag)
	{
		CO2_RxFlag=0;
		if(GetCO2Value(recvCO2_buf,&CO2)==1)
		{
			printf("CO2浓度:%d ppm\r\n",CO2);
		}
	}
	HAL_Delay(1000);
}



/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
