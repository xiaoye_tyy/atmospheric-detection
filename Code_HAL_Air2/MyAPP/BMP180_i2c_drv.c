#include "BMP180_i2c_drv.h"
#include "i2c.h"
#include "stdio.h"


// BMP180 硬件IIC 测试完成
typedef struct bmp180_e2prom_param {
	short AC1;
	short AC2;
	short AC3;
	unsigned short AC4;
	unsigned short AC5;
	unsigned short AC6;
	short B1;
	short B2;
	short MB;
	short MC;
	short MD;
} bmp180_e2prom_param;

const uint8_t DEVICE_ADDER = 0xee;
const uint8_t CTRL_MEAS = 0xf4;
const uint8_t EEPROM_ADDR_MSB[11] = { 0xaa, 0xac, 0xae, 0xb0, 0xb2, 0xb4, 0xb6, 0xb8, 0xba, 0xbc, 0xbe };
const uint8_t EEPROM_ADDR_LSB[11] = { 0xab, 0xad, 0xaf, 0xb1, 0xb3, 0xb5, 0xb7, 0xb9, 0xbb, 0xbd, 0xbf };

bmp180_e2prom_param bmp_param;

uint16_t Reg_Read16(uint8_t MSB , uint8_t LSB){
		uint8_t data[2];
		HAL_I2C_Mem_Read (&hi2c2 ,DEVICE_ADDER,MSB ,1,&data[0] ,1,1000);
		HAL_I2C_Mem_Read (&hi2c2 ,DEVICE_ADDER,LSB ,1,&data[1] ,1,1000);
		return (data[0] << 8) | data[1];
}

uint32_t Reg_Read32(uint8_t MSB , uint8_t LSB, uint8_t XLSB){
		uint8_t data[3];
		HAL_I2C_Mem_Read (&hi2c2 ,DEVICE_ADDER,MSB ,1,&data[0] ,1,1000);
		HAL_I2C_Mem_Read (&hi2c2 ,DEVICE_ADDER,LSB ,1,&data[1] ,1,1000);
		HAL_I2C_Mem_Read (&hi2c2 ,DEVICE_ADDER,XLSB ,1,&data[2] ,1,1000);
		return (data[0] << 16) | (data[1] << 8) | data[2];
}

void bmp180_get_cal_param(){
		bmp_param.AC1 = Reg_Read16(EEPROM_ADDR_MSB[0],EEPROM_ADDR_LSB[0]);
		bmp_param.AC2 = Reg_Read16(EEPROM_ADDR_MSB[1],EEPROM_ADDR_LSB[1]);
		bmp_param.AC3 = Reg_Read16(EEPROM_ADDR_MSB[2],EEPROM_ADDR_LSB[2]);
		bmp_param.AC4 = Reg_Read16(EEPROM_ADDR_MSB[3],EEPROM_ADDR_LSB[3]);
		bmp_param.AC5 = Reg_Read16(EEPROM_ADDR_MSB[4],EEPROM_ADDR_LSB[4]);
		bmp_param.AC6 = Reg_Read16(EEPROM_ADDR_MSB[5],EEPROM_ADDR_LSB[5]);
		bmp_param.B1 = Reg_Read16(EEPROM_ADDR_MSB[6],EEPROM_ADDR_LSB[6]);
		bmp_param.B2 = Reg_Read16(EEPROM_ADDR_MSB[7],EEPROM_ADDR_LSB[7]);
		bmp_param.MB = Reg_Read16(EEPROM_ADDR_MSB[8],EEPROM_ADDR_LSB[8]);
		bmp_param.MC = Reg_Read16(EEPROM_ADDR_MSB[9],EEPROM_ADDR_LSB[9]);
		bmp_param.MD = Reg_Read16(EEPROM_ADDR_MSB[10],EEPROM_ADDR_LSB[10]);
}
void GetRawData(float *pressure){
	
			//BMP180初始化
		bmp180_get_cal_param();
		HAL_Delay(500);
		uint8_t data = 0x2e;
	
		HAL_I2C_Mem_Write (&hi2c2 ,DEVICE_ADDER ,CTRL_MEAS ,1,&data ,1,1000);	
		
		HAL_Delay(10);	
	
		int32_t UT = Reg_Read16(0xf6,0xf7);
		
	
		int32_t X1 = (UT-bmp_param.AC6)* bmp_param.AC5 >> 15;
	
		int32_t X2 = (bmp_param.MC << 11) / (X1 + bmp_param.MD);
	
		int32_t T = (X1 + X2 +8) >> 4 ;
	
	  data = 0x74;
	
		HAL_I2C_Mem_Write (&hi2c2 ,DEVICE_ADDER ,CTRL_MEAS ,1,&data ,1,1000);
	
		HAL_Delay(10);
	
		int32_t UP = Reg_Read32(0xf6,0xf7,0xf8) >> 8 ;
		
		int32_t B6 = X1 + X2 - 4000;
		
		X1 = (B6 * B6 >> 12) * bmp_param.B2 >> 11;
		
		X2 = bmp_param.AC2 * B6 >> 11;
		
		int32_t X3 = X1 + X2;
		
		int32_t B3 = (((bmp_param.AC1 << 2) + X3) + 2) >> 2;

		X1 = bmp_param.AC3 * B6 >> 13;
		
		X2 = (B6 * B6 >> 12) * bmp_param.B1 >> 16;
		
		X3 = (X1 + X2 + 2) >> 2; 
		
		uint32_t B4 = bmp_param.AC4 * (uint32_t)(X3 + 32768) >> 15;

		uint32_t B7 = ((uint32_t)UP - B3) * 50000;
		
		int32_t p;
		if(B7 < 0x80000000){
				p = (B7 << 1) / B4;  
		}else{
				p = B7/B4 << 1;
		}
		
		X1 = (p >> 8) * (p >> 8);
		
		X1 = (X1 * 3038) >> 16;
		
		X2 = (-7375 * p) >> 16;
		
		p = p + ((X1 + X2 + 3791) >> 4);
		
		float altitude = (float)(101325 - p) / 133 * 12;

		printf("温度：%.2f,气压：%.3f kPa",(float)T/10,(float)p/1000);
		
		*pressure = (float)p/1000;
}